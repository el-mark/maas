Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'dashboard#index'
  get 'edit', to: 'dashboard#edit'

  namespace :api, constraints: { format: 'json' } do
    post 'obtain_turn_data', to: 'dashboard#obtain_turn_data'
    post 'turn_confirmation', to: 'dashboard#turn_confirmation'
  end
end
