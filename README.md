# Get Started

To get the app running you will need to clone the repo and use this versions:

* ruby '2.5.0'

* gem 'rails', '~> 5.2.2'

Then you need to run

`bundle install`

`yarn install`

`rails db:create db:migrate db:seed`

