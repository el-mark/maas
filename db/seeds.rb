# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Engineer.where(name: 'Benjamín', color: '577590').first_or_create
Engineer.where(name: 'Bárbara', color: 'f94144').first_or_create
Engineer.where(name: 'Ernesto', color: '43aa8b').first_or_create

# Populate "recorrido.cl" schedule
service = Service.where(name: 'Recorrido.cl').first_or_create
(1..5).each do |day_of_week|
  (19..23).each do |hour|
    Schedule.where(
      day_of_week: day_of_week,
      hour: hour,
      service_id: service
    ).first_or_create
  end
end
[6, 0].each do |day_of_week|
  (10..23).each do |hour|
    Schedule.where(
      day_of_week: day_of_week,
      hour: hour,
      service_id: service
    ).first_or_create
  end
end

# Populate "otra.empresa" schedule
service_extra = Service.where(name: 'otra.empresa').first_or_create
(1..2).each do |day_of_week|
  (17..23).each do |hour|
    Schedule.where(
      day_of_week: day_of_week,
      hour: hour,
      service_id: service_extra
    ).first_or_create
  end
end
