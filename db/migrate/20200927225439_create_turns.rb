class CreateTurns < ActiveRecord::Migration[5.2]
  def change
    create_table :turns do |t|
      t.belongs_to :engineer
      t.belongs_to :service
      t.date :turn_date
      t.integer :hour
      t.boolean :confirmed

      t.timestamps
    end
  end
end
