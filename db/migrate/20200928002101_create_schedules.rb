class CreateSchedules < ActiveRecord::Migration[5.2]
  def change
    create_table :schedules do |t|
      t.belongs_to :service
      t.integer :day_of_week
      t.integer :hour

      t.timestamps
    end
  end
end
