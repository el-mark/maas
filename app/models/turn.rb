class Turn < ApplicationRecord
  belongs_to :engineer
  belongs_to :service

  validates :turn_date, :hour, presence: true
end
