class DashboardController < ApplicationController
  def index
    days_to_sum = (8 - Time.now.wday) % 7
    starting_date = Time.now + days_to_sum.days

    days_to_sum = (8 - Time.now.wday) % 7
    starting_date = Time.now + days_to_sum.days
    service = Service.first

    weeks = []
    (0..4).each do |index|
      week_date = starting_date + index.weeks
      week_number = week_date.to_date.cweek

      weeks.push(
        week_number: week_number,
        year: week_date.year,
        week_text: "Semana #{week_number} del #{week_date.year}"
      )
    end

    services = []
    Service.all.each do |service|
      services.push(
        service_id: service.id,
        name: service.name
      )
    end

    render locals: {
      weeks: weeks,
      services: services,
      turns_data: turns_data(starting_date, service),
      starting_date: starting_date,
      engineers: Engineer.all,
      service: Service.first
    }
  end

  def edit
    days_to_sum = (8 - Time.now.wday) % 7
    starting_date = Time.now + days_to_sum.days
    service = Service.first

    weeks = []
    (0..4).each do |index|
      week_date = starting_date + index.weeks
      week_number = week_date.to_date.cweek

      weeks.push(
        week_number: week_number,
        year: week_date.year,
        week_text: "Semana #{week_number} del #{week_date.year}"
      )
    end

    services = []
    Service.all.each do |service|
      services.push(
        service_id: service.id,
        name: service.name
      )
    end

    render locals: {
      weeks: weeks,
      services: services,
      turns_data: turns_data(starting_date, service)
    }
  end
end
