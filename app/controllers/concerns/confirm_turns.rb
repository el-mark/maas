module ConfirmTurns
  extend ActiveSupport::Concern

  def confirm_turns(beginning_of_week, service_id)
    engineer_hash = {}
    Engineer.all.each do |engineer|
      engineer_hash[engineer.id] = 0
    end

    (0..6).each do |week_day|
      turns = Turn.where(
        service_id: service_id,
        turn_date: beginning_of_week + week_day.days
      )
      hours = turns.pluck('hour').uniq.sort

      hours.each do |hour|
        engineer_hash = engineer_hash.sort_by { |_, v| v }.to_h
        selected = 0
        engineer_hash.each do |k, v|
          turn = turns.where(hour: hour, engineer_id: k).first
          if turn.present? && selected.zero?
            turn.update(confirmed: true)
            selected = 1
            engineer_hash[k] = v + 1
          elsif turn.present?
            turn.update(confirmed: false)
          end
        end
      end
    end
  end
end
