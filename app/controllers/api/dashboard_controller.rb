class Api::DashboardController < ApplicationController
  skip_before_action :verify_authenticity_token
  include ConfirmTurns

  def turn_confirmation
    starting_date = Date.parse(
      "#{params[:year]}W#{params[:week]}"
    )
    turn = Turn.where(
      hour: params[:hour],
      engineer_id: params[:engineer_id],
      service_id: params[:service_id],
      turn_date: params[:turn_date]
    ).first_or_initialize
    beginning_of_week = turn.turn_date.beginning_of_week
    service_id = turn.service_id
    if params[:turn_selected] && turn.save
      confirm_turns(beginning_of_week, service_id)
      render json: turns_data(starting_date, Service.find(params[:service_id]))
    elsif turn.destroy
      confirm_turns(beginning_of_week, service_id)
      render json: turns_data(starting_date, Service.find(params[:service_id]))
    else
      render json: { status: :unprocessable_entity }
    end
  end

  def obtain_turn_data
    starting_date = Date.parse(
      "#{params[:year]}W#{params[:week]}"
    )

    render json: turns_data(starting_date, Service.find(params[:service_id]))
  end
end
