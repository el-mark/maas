class ApplicationController < ActionController::Base
  def turns_data(starting_date, service)
    turns = []
    engineers = Engineer.all
    (0..6).each do |week_day|
      schedule_date = starting_date + week_day.days

      engineers_array = []
      engineers.each do |engineer|
        count = Turn.where(engineer_id: engineer.id, service_id: service.id, confirmed: true).where('turn_date between ? and ?', starting_date.to_date, starting_date.to_date + 6.days).count
        engineers_array.push(
          engineerId: engineer.id,
          color: engineer.color,
          name: engineer.name,
          count: count
        )
      end

      schedule_hours_array = []
      schedule_hours = service.schedules.where(day_of_week: schedule_date.wday)
      schedule_hours.each do |schedule_hour|
        schedule_engineers_array = []
        confirmed_engineer = false
        engineers.each do |engineer|
          turn = Turn.where(
            engineer_id: engineer.id, service_id: service.id,
            hour: schedule_hour.hour, turn_date: schedule_date.to_date
          )
          schedule_engineers_array.push(
            engineerId: engineer.id,
            selected: turn.present?,
            serviceId: service.id,
            name: engineer.name,
            confirmed: turn.where(confirmed: true).present?,
            color: engineer.color
          )
          confirmed_engineer = true if turn.where(confirmed: true).present?
        end

        schedule_hours_array.push(
          hour: schedule_hour.hour,
          status: turn_hour_status(service, schedule_date, schedule_hour),
          engineers: schedule_engineers_array,
          confirmed_engineer: confirmed_engineer
        )
      end

      turns.push(
        weekDay: week_day,
        turnDate: schedule_date.to_date,
        dateString: I18n.l(schedule_date, format: '%A %d de %B'),
        engineers: engineers_array,
        scheduleHours: schedule_hours_array
      )
    end
    turns
  end

  def turn_hour_status(service, schedule_date, schedule_hour)
    turns = Turn.where(
      service_id: service.id, hour: schedule_hour.hour,
      turn_date: schedule_date.to_date
    )
    turns.present? ? 'occupied' : 'empty'
  end
end
