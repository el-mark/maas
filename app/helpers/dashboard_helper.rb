module DashboardHelper
  def turn_hour_color(service, schedule_date, schedules_hour)
    turns = Turn.where(
      service_id: service.id, hour: schedules_hour.hour, 
      turn_date: schedule_date.to_date
    )
    turns.present? ? '90be6d' : 'f8961e'
  end
end
