import React from "react"
import IndexTable from "./IndexTable"

class IndexGrid extends React.Component {
  render () {
    return (
      <div className="row">
        <div className="col-4 count-table">
          <table>
            <tbody>
              {
                this.props.turnsData[0].engineers.map(engineer => {
                  return(
                    <tr key={engineer.engineerId}>
                      <td className="count-table-name" style={{backgroundColor: '#' + engineer.color}}>
                        {engineer.name}
                      </td>
                      <td className="count-table-count">
                        {engineer.count}
                      </td>
                    </tr>
                  )
                })
              }
            </tbody>
          </table>
        </div>
        {
          this.props.turnsData.map(turn => {
            return(
              <IndexTable 
                key={turn.weekDay}
                turnDate={turn.turnDate}
                dateString={turn.dateString}
                engineers={turn.engineers}
                scheduleHours={turn.scheduleHours}
                handleSelectTurn={this.props.handleSelectTurn}
              />
            )
          })
        }
      </div>
    );
  }
}

export default IndexGrid
