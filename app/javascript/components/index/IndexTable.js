import React from "react"
import TurnSelected from "./TurnSelected"

class IndexTable extends React.Component {
  render () {
    return(
      <div className="col-4 edit-turn-table">
        <table>
          <thead>
            <tr>
              <th className="date-header" colSpan="2">
                {this.props.dateString}
              </th>
            </tr>
          </thead>
          <TurnSelected
            scheduleHours={this.props.scheduleHours}
            turnDate={this.props.turnDate}
            handleSelectTurn={this.props.handleSelectTurn}
          />
        </table>
      </div>
    );
  }
}

export default IndexTable
