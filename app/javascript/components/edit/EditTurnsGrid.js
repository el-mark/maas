import React from "react"
import EditTurnsTable from "./EditTurnsTable"

class EditTurnsGrid extends React.Component {
  render () {
    return (
      <div className="row">
        {
          this.props.turnsData.map(turn => {
            return(
              <EditTurnsTable 
                key={turn.weekDay}
                turnDate={turn.turnDate}
                dateString={turn.dateString}
                engineers={turn.engineers}
                scheduleHours={turn.scheduleHours}
                handleSelectTurn={this.props.handleSelectTurn}
              />
            )
          })
        }
      </div>
    );
  }
}

export default EditTurnsGrid
