import React from "react"
import TurnSelector from "./TurnSelector"

class EditTurnsTable extends React.Component {
  render () {
    return(
      <div className="col-6 edit-turn-table">
        <table>
          <thead>
            <tr>
              <th className="date-header">
                {this.props.dateString}
              </th>
              {
                this.props.engineers.map(engineer => {
                  return(
                    <th
                      className="engineer-cell"
                      style={{backgroundColor: '#' + engineer.color}}
                      key={engineer.engineerId}
                    >
                      {engineer.name}
                    </th>
                  )
                })
              }
            </tr>
          </thead>
          <TurnSelector
            scheduleHours={this.props.scheduleHours}
            turnDate={this.props.turnDate}
            handleSelectTurn={this.props.handleSelectTurn}
          />
        </table>
      </div>
    );
  }
}

export default EditTurnsTable
