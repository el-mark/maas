import React from "react"

class ServiceAndWeek extends React.Component {
  render () {
    return (
      <div className="row">
        <div className="col-4 basic-padding">
          <p>
            <select
              onChange={this.props.handleServiceSelect}
              id="service-select"
            >
              {
                this.props.services.map(service => {
                  return(
                    <option 
                      key={service.service_id} 
                      value={service.service_id}
                      data-service-id={service.service_id}
                    >
                      {service.name}
                    </option>
                  )
                })
              }
            </select>
            <br />
            <select
              onChange={this.props.handleWeekSelect}
              id="week-select"
            >
              {
                this.props.weeks.map(week => {
                  return(
                    <option 
                      key={week.week_number} 
                      value={week.week_number}
                      data-year={week.year}
                      data-week-number={week.week_number}
                    >
                      {week.week_text}
                    </option>
                  )
                })
              }
            </select>
            <br />
            del {this.props.turnsData[0].turnDate.replaceAll("-", "/")} al {this.props.turnsData[6].turnDate.replaceAll("-", "/")}
          </p>
        </div>
        <div className="col-4"></div>
        <div className="col-4 basic-padding">
          <a href={this.props.rootPath} className="back-button">
            Volver
          </a>
        </div>
      </div>
    );
  }
}

export default ServiceAndWeek
