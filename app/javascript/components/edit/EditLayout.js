import React from "react"
import ServiceAndWeek from "./ServiceAndWeek";
import EditTurnsGrid from "./EditTurnsGrid";

class EditLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      turnsData: props.turnsData
    }
  }

  handleSelectTurn = (event) => {
    const weekSelect = document.getElementById("week-select");
    const serviceSelect = document.getElementById("service-select");
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        turn_selected: event.target.checked,
        engineer_id: event.target.dataset.engineerId,
        hour: event.target.dataset.hour,
        turn_date: event.target.dataset.turnDate,
        service_id: event.target.dataset.serviceId,
        year: weekSelect.options[weekSelect.selectedIndex].dataset.year,
        week: weekSelect.options[weekSelect.selectedIndex].dataset.weekNumber,
        service_id: serviceSelect.options[serviceSelect.selectedIndex].dataset.serviceId
      })
    };
    this.refresh_turn_data('/api/turn_confirmation', requestOptions)
  }

  handleServiceSelect = (event) => {
    const weekSelect = document.getElementById("week-select");
    const body = JSON.stringify({
      year: weekSelect.options[weekSelect.selectedIndex].dataset.year,
      week: weekSelect.options[weekSelect.selectedIndex].dataset.weekNumber,
      service_id: event.target.options[event.target.selectedIndex].dataset.serviceId
    })
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: body
    };
    this.refresh_turn_data('/api/obtain_turn_data', requestOptions)
  }

  handleWeekSelect = (event) => {
    const serviceSelect = document.getElementById("service-select");
    const body = JSON.stringify({
      year: event.target.options[event.target.selectedIndex].dataset.year,
      week: event.target.options[event.target.selectedIndex].dataset.weekNumber,
      service_id: serviceSelect.options[serviceSelect.selectedIndex].dataset.serviceId
    })
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: body
    };
    this.refresh_turn_data('/api/obtain_turn_data', requestOptions)
  }

  refresh_turn_data = (url, requestOptions) => {
    function status(response) {
      if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response)
      } else {
        return Promise.reject(new Error(response.statusText))
      }
    }
    function json(response) {
      return response.json()
    }
    
    fetch(url, requestOptions)
      .then(status)
      .then(json)
      .then(data => {
        this.setState({turnsData: data})
      })
      .catch((error) => {
        console.error('Error:', error)
      });
  }

  render () {
    return (
      <div className="container container-margin">
        <ServiceAndWeek
          rootPath={this.props.rootPath} 
          weeks={this.props.weeks}
          services={this.props.services}
          turnsData={this.state.turnsData} 
          handleWeekSelect={this.handleWeekSelect}
          handleServiceSelect={this.handleServiceSelect}
        />
        <EditTurnsGrid 
          turnsData={this.state.turnsData}
          handleSelectTurn={this.handleSelectTurn}
        />
      </div>
    );
  }
}

export default EditLayout
