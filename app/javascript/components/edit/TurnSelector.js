import React from "react"

class TurnSelector extends React.Component {
  render () {
    return (
      <tbody>
        {
          this.props.scheduleHours.map(scheduleHour => {
            return(
              <tr key={scheduleHour.hour}>
                <td
                  className={"schedule-hour schedule-hour__" + scheduleHour.status}
                >
                  {scheduleHour.hour} - {scheduleHour.hour + 1}
                </td>
                {
                  scheduleHour.engineers.map(engineer => {
                    return(
                      <td key={engineer.engineerId} className="checkbox-cell" >
                        <input 
                          type="checkbox"
                          checked={engineer.selected}
                          onChange={this.props.handleSelectTurn}
                          data-engineer-id={engineer.engineerId}
                          data-hour={scheduleHour.hour}
                          data-turn-date={this.props.turnDate}
                          data-service-id={engineer.serviceId}
                        />
                      </td>
                    )
                  })
                }
              </tr>
            )
          })
        }
      </tbody>
    );
  }
}

export default TurnSelector
